#pragma once

#include "Figure.h"

namespace hse_graphics_1_brez
{
	// ����� ��� ��������� �������
	private ref class Ellipse : Figure 
	{
	public:
		static bool drawRectangle = false;
		static Pen^ rectanglePen = Pens::Gainsboro;

		// �������������� ����� ���������
		Void DrawFigure() override
		{
			int cx = (point1->X + point2->X) / 2;
			int cy = (point1->Y + point2->Y) / 2;

			int dx = Math::Abs((point1->X - point2->X) / 2);
			int dy = Math::Abs((point1->Y - point2->Y) / 2);

			// ������ ������������� ������ ������, ���� ����
			if (drawRectangle)
				DrawRectangle(rectanglePen, point1, point2);

			// ������ ������ ���������� �������� Graphics
			if (builtinShow)
				g->DrawEllipse(
					builtinPen,
					cx - dx + builtinOffset->X,
					cy - dy + builtinOffset->Y,
					dx + dx,
					dy + dy);

			// ������ ������ ����� ��������
			DrawEllipse(cx, cy, dx, dy);
		}
	};
}
