#pragma once

namespace hse_graphics_1_brez 
{
	using namespace System;
	using namespace System::Drawing;
	using namespace System::Collections;

	ref class DrawingContainer;

	// ������� ����� ��� ��������� �����
	ref class Figure 
	{
	protected:
		ArrayList^ uniqueYPoints = gcnew ArrayList;
		ArrayList^ drawnPoints = gcnew ArrayList;
		int lastDrawnPointY;
		bool isDrawing = false;

		// ����� ��� ���������������
		virtual Void DrawFigure() { }

	public:

		virtual bool MouseDown(Point^ loc) { return false; }
		virtual bool MouseUp(Point^ loc, bool rClick) { return false; }
		virtual bool MouseMove(Point^ loc) { return false; }

		// ���������� ����� �������
		static double Distance(Point^ a, Point^ b)
		{
			int x = a->X - b->X;
			int y = a->Y - b->Y;
			return Math::Sqrt(x*x + y*y);
		}

		// ���� - ����� �� �������� ������ �������������� � ������� ���������� ������� Graphics
		static bool builtinShow = false;
		static Pen^ builtinPen = Pens::Blue;
		static Point^ builtinOffset = gcnew Point(5, 0);

		Graphics^ g;
		Pen^ p;
		Point^ point1;
		Point^ point2;

		bool canBeFilled = true;

		DrawingContainer^ dc;

		Void Draw();

		Void Draw(Color^ fillColor);

		Void Fill(Color^ fillColor);

		// ���������� �����
		Void DrawPoint(int x, int y);

		// ���������� ������������� � ������� Graphics::DrawRectangle
		Void DrawRectangle(Pen^ pen, Point^ p1, Point^ p2);

		// ���������� �����
		Void DrawLine(Point^ from, Point^ to);

		// ���������� ������
		Void DrawEllipse(int cx, int cy, int a, int b);

		Rectangle^ RectForPoints(Point^ p1, Point^ p2);
	};
}