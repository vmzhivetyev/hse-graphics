#pragma once

#include "Figure.h"

namespace hse_graphics_1_brez
{
	// ����� ��� ��������� �����
	ref class Line : Figure
	{
	public:
		Line()
		{
			canBeFilled = false;
		}

		// �������������� ����� ���������
		Void DrawFigure() override
		{
			// ������ ����� ����� ��������
			DrawLine(point1, point2);

			// ������ ����� ���������� �������� Graphics
			if (builtinShow)
			{
				int offset = 5;
				g->DrawLine(
					builtinPen,
					point1->X + builtinOffset->X,
					point1->Y + builtinOffset->Y,
					point2->X + builtinOffset->X,
					point2->Y + builtinOffset->Y);
			}
		}
	};
}


