#pragma once

#include "Figure.h"

namespace hse_graphics_1_brez
{
	// ����� ��� ��������� ����������
	ref class Circle : Figure
	{

	public:

		// �������������� ����� ���������
		Void DrawFigure() override
		{
			int r = Distance(point1, point2);

			// ������ ���������� ���������� �������� Graphics
			if (builtinShow)
			{
				g->DrawEllipse(
					builtinPen,
					point1->X - r + builtinOffset->X,
					point1->Y - r + builtinOffset->Y,
					r + r,
					r + r);
			}

			// ������ ���������� ����� ��������
			DrawEllipse(point1->X, point1->Y, r, r);
		}
	};
}

