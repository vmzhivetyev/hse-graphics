#pragma once

/*
 ��������:	�������� ��������, ���-153

 ����� ����������:	Visual Studio 2017
 �������:			Windows 10
 ��������� ����������:
	CPU: Intel Core i5 7600K 4.8GHz
	GPU: Asus GeForce 1060 6GB

 ���� � ��� ����������� - ����� �� ��������� 
	[Help]->[About]
*/

#include "Line.h"
#include "Circle.h"
#include "Ellipse.h"
#include "DrawingContainer.h"
#include "Filling.h"
#include "Polygon.h"
#include "LineClipper.h"
#include "Bezier.h"
#include "BezierComplex.h"
#include "BSpline.h"

namespace hse_graphics_1_brez 
{
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			Start();
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::MenuStrip^  menuStrip1;


	private: System::Windows::Forms::ToolStripComboBox^  toolStripComboBox1;
	private: System::Windows::Forms::ToolStripMenuItem^  editToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  undoToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  clearToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  helpToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  aboutToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  figureToolStripMenuItem;


	private: System::Windows::Forms::ToolStripMenuItem^  settingsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  drawBuiltinMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  drawRectangleMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  toolsToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  generateRandomFiguresToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  verboseToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  drawPixelsForFillingBetweenToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  fillFiguresToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  clippingModeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripSeparator^  toolStripSeparator1;
	private: System::Windows::Forms::StatusStrip^  statusStrip1;

	private: System::Windows::Forms::ToolStripMenuItem^  colorToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  colorToolStripMenuItem1;
	private: System::Windows::Forms::ColorDialog^  colorDialog1;
	private: System::Windows::Forms::ToolStripMenuItem^  editModeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripStatusLabel^  toolStripStatusLabel1;



	protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->editToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->undoToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->clearToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripComboBox1 = (gcnew System::Windows::Forms::ToolStripComboBox());
			this->settingsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->drawBuiltinMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->drawRectangleMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->verboseToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->drawPixelsForFillingBetweenToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->fillFiguresToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->clippingModeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolsToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->generateRandomFiguresToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->helpToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->aboutToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->figureToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->colorToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->colorToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->editModeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
			this->toolStripStatusLabel1 = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->colorDialog1 = (gcnew System::Windows::Forms::ColorDialog());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->menuStrip1->SuspendLayout();
			this->statusStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->BackColor = System::Drawing::SystemColors::Control;
			this->pictureBox1->Cursor = System::Windows::Forms::Cursors::Cross;
			this->pictureBox1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->pictureBox1->Location = System::Drawing::Point(0, 29);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(748, 512);
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::pictureBox1_MouseDown);
			this->pictureBox1->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::pictureBox1_MouseMove);
			this->pictureBox1->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &MyForm::pictureBox1_MouseUp);
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(9) {
				this->editToolStripMenuItem,
					this->toolStripComboBox1, this->settingsToolStripMenuItem, this->toolsToolStripMenuItem, this->helpToolStripMenuItem, this->figureToolStripMenuItem,
					this->colorToolStripMenuItem, this->colorToolStripMenuItem1, this->editModeToolStripMenuItem
			});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(748, 29);
			this->menuStrip1->TabIndex = 1;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// editToolStripMenuItem
			// 
			this->editToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->undoToolStripMenuItem,
					this->clearToolStripMenuItem
			});
			this->editToolStripMenuItem->Name = L"editToolStripMenuItem";
			this->editToolStripMenuItem->Size = System::Drawing::Size(39, 25);
			this->editToolStripMenuItem->Text = L"Edit";
			// 
			// undoToolStripMenuItem
			// 
			this->undoToolStripMenuItem->Name = L"undoToolStripMenuItem";
			this->undoToolStripMenuItem->ShortcutKeys = static_cast<System::Windows::Forms::Keys>((System::Windows::Forms::Keys::Control | System::Windows::Forms::Keys::Z));
			this->undoToolStripMenuItem->Size = System::Drawing::Size(144, 22);
			this->undoToolStripMenuItem->Text = L"Undo";
			this->undoToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::undoToolStripMenuItem_Click);
			// 
			// clearToolStripMenuItem
			// 
			this->clearToolStripMenuItem->Name = L"clearToolStripMenuItem";
			this->clearToolStripMenuItem->Size = System::Drawing::Size(144, 22);
			this->clearToolStripMenuItem->Text = L"Clear";
			this->clearToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::clearToolStripMenuItem_Click);
			// 
			// toolStripComboBox1
			// 
			this->toolStripComboBox1->Alignment = System::Windows::Forms::ToolStripItemAlignment::Right;
			this->toolStripComboBox1->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->toolStripComboBox1->FlatStyle = System::Windows::Forms::FlatStyle::System;
			this->toolStripComboBox1->Items->AddRange(gcnew cli::array< System::Object^  >(9) {
				L"Line", L"Circle", L"Ellipse", L"Fill",
					L"Polygon", L"Clipping Area", L"Simple Bezier", L"Complex Bezier", L"B-Spline (w\\ complex)"
			});
			this->toolStripComboBox1->Name = L"toolStripComboBox1";
			this->toolStripComboBox1->Size = System::Drawing::Size(121, 25);
			this->toolStripComboBox1->ToolTipText = L"Drawing Mode";
			this->toolStripComboBox1->SelectedIndexChanged += gcnew System::EventHandler(this, &MyForm::toolStripComboBox1_SelectedIndexChanged);
			// 
			// settingsToolStripMenuItem
			// 
			this->settingsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(7) {
				this->drawBuiltinMenuItem,
					this->drawRectangleMenuItem, this->verboseToolStripMenuItem, this->drawPixelsForFillingBetweenToolStripMenuItem, this->toolStripSeparator1,
					this->fillFiguresToolStripMenuItem, this->clippingModeToolStripMenuItem
			});
			this->settingsToolStripMenuItem->Name = L"settingsToolStripMenuItem";
			this->settingsToolStripMenuItem->Size = System::Drawing::Size(61, 25);
			this->settingsToolStripMenuItem->Text = L"Settings";
			// 
			// drawBuiltinMenuItem
			// 
			this->drawBuiltinMenuItem->Name = L"drawBuiltinMenuItem";
			this->drawBuiltinMenuItem->Size = System::Drawing::Size(311, 22);
			this->drawBuiltinMenuItem->Text = L"Draw Figures With Builtin Graphics Functions";
			this->drawBuiltinMenuItem->Click += gcnew System::EventHandler(this, &MyForm::drawFiguresWithBuiltinGraphicsFunctionsToolStripMenuItem_Click);
			// 
			// drawRectangleMenuItem
			// 
			this->drawRectangleMenuItem->Name = L"drawRectangleMenuItem";
			this->drawRectangleMenuItem->Size = System::Drawing::Size(311, 22);
			this->drawRectangleMenuItem->Text = L"Draw Rectangle Around Ellipse";
			this->drawRectangleMenuItem->Click += gcnew System::EventHandler(this, &MyForm::drawRectangleAroundEllipseToolStripMenuItem_Click);
			// 
			// verboseToolStripMenuItem
			// 
			this->verboseToolStripMenuItem->Name = L"verboseToolStripMenuItem";
			this->verboseToolStripMenuItem->Size = System::Drawing::Size(311, 22);
			this->verboseToolStripMenuItem->Text = L"Draw Pixel By Pixel";
			this->verboseToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::verboseToolStripMenuItem_Click);
			// 
			// drawPixelsForFillingBetweenToolStripMenuItem
			// 
			this->drawPixelsForFillingBetweenToolStripMenuItem->Name = L"drawPixelsForFillingBetweenToolStripMenuItem";
			this->drawPixelsForFillingBetweenToolStripMenuItem->Size = System::Drawing::Size(311, 22);
			this->drawPixelsForFillingBetweenToolStripMenuItem->Text = L"Draw Pixels For Filling Between";
			this->drawPixelsForFillingBetweenToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::drawPixelsForFillingBetweenToolStripMenuItem_Click);
			// 
			// toolStripSeparator1
			// 
			this->toolStripSeparator1->Name = L"toolStripSeparator1";
			this->toolStripSeparator1->Size = System::Drawing::Size(308, 6);
			// 
			// fillFiguresToolStripMenuItem
			// 
			this->fillFiguresToolStripMenuItem->Name = L"fillFiguresToolStripMenuItem";
			this->fillFiguresToolStripMenuItem->Size = System::Drawing::Size(311, 22);
			this->fillFiguresToolStripMenuItem->Text = L"Fill Figures";
			this->fillFiguresToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::fillFiguresToolStripMenuItem_Click);
			// 
			// clippingModeToolStripMenuItem
			// 
			this->clippingModeToolStripMenuItem->Name = L"clippingModeToolStripMenuItem";
			this->clippingModeToolStripMenuItem->Size = System::Drawing::Size(311, 22);
			this->clippingModeToolStripMenuItem->Text = L"Clipping Mode";
			this->clippingModeToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::clippingModeToolStripMenuItem_Click);
			// 
			// toolsToolStripMenuItem
			// 
			this->toolsToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->generateRandomFiguresToolStripMenuItem });
			this->toolsToolStripMenuItem->Name = L"toolsToolStripMenuItem";
			this->toolsToolStripMenuItem->Size = System::Drawing::Size(47, 25);
			this->toolsToolStripMenuItem->Text = L"Tools";
			// 
			// generateRandomFiguresToolStripMenuItem
			// 
			this->generateRandomFiguresToolStripMenuItem->Name = L"generateRandomFiguresToolStripMenuItem";
			this->generateRandomFiguresToolStripMenuItem->Size = System::Drawing::Size(205, 22);
			this->generateRandomFiguresToolStripMenuItem->Text = L"Generate random figures";
			this->generateRandomFiguresToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::generateRandomFiguresToolStripMenuItem_Click);
			// 
			// helpToolStripMenuItem
			// 
			this->helpToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->aboutToolStripMenuItem });
			this->helpToolStripMenuItem->Name = L"helpToolStripMenuItem";
			this->helpToolStripMenuItem->Size = System::Drawing::Size(44, 25);
			this->helpToolStripMenuItem->Text = L"Help";
			// 
			// aboutToolStripMenuItem
			// 
			this->aboutToolStripMenuItem->Name = L"aboutToolStripMenuItem";
			this->aboutToolStripMenuItem->Size = System::Drawing::Size(107, 22);
			this->aboutToolStripMenuItem->Text = L"About";
			this->aboutToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::aboutToolStripMenuItem_Click);
			// 
			// figureToolStripMenuItem
			// 
			this->figureToolStripMenuItem->Alignment = System::Windows::Forms::ToolStripItemAlignment::Right;
			this->figureToolStripMenuItem->Enabled = false;
			this->figureToolStripMenuItem->Name = L"figureToolStripMenuItem";
			this->figureToolStripMenuItem->ShowShortcutKeys = false;
			this->figureToolStripMenuItem->Size = System::Drawing::Size(55, 25);
			this->figureToolStripMenuItem->Text = L"Figure:";
			// 
			// colorToolStripMenuItem
			// 
			this->colorToolStripMenuItem->Alignment = System::Windows::Forms::ToolStripItemAlignment::Right;
			this->colorToolStripMenuItem->BackColor = System::Drawing::Color::Black;
			this->colorToolStripMenuItem->Margin = System::Windows::Forms::Padding(3);
			this->colorToolStripMenuItem->Name = L"colorToolStripMenuItem";
			this->colorToolStripMenuItem->Size = System::Drawing::Size(25, 19);
			this->colorToolStripMenuItem->Text = L"  ";
			this->colorToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::colorToolStripMenuItem_Click);
			// 
			// colorToolStripMenuItem1
			// 
			this->colorToolStripMenuItem1->Alignment = System::Windows::Forms::ToolStripItemAlignment::Right;
			this->colorToolStripMenuItem1->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
			this->colorToolStripMenuItem1->Enabled = false;
			this->colorToolStripMenuItem1->Name = L"colorToolStripMenuItem1";
			this->colorToolStripMenuItem1->ShowShortcutKeys = false;
			this->colorToolStripMenuItem1->Size = System::Drawing::Size(51, 25);
			this->colorToolStripMenuItem1->Text = L"Color:";
			// 
			// editModeToolStripMenuItem
			// 
			this->editModeToolStripMenuItem->Alignment = System::Windows::Forms::ToolStripItemAlignment::Right;
			this->editModeToolStripMenuItem->BackColor = System::Drawing::SystemColors::Control;
			this->editModeToolStripMenuItem->CheckOnClick = true;
			this->editModeToolStripMenuItem->ForeColor = System::Drawing::SystemColors::ControlText;
			this->editModeToolStripMenuItem->Name = L"editModeToolStripMenuItem";
			this->editModeToolStripMenuItem->Size = System::Drawing::Size(73, 25);
			this->editModeToolStripMenuItem->Text = L"Edit Mode";
			this->editModeToolStripMenuItem->ToolTipText = L"Edit Mode";
			this->editModeToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyForm::editModeToolStripMenuItem_Click);
			// 
			// statusStrip1
			// 
			this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->toolStripStatusLabel1 });
			this->statusStrip1->Location = System::Drawing::Point(0, 519);
			this->statusStrip1->Name = L"statusStrip1";
			this->statusStrip1->Size = System::Drawing::Size(748, 22);
			this->statusStrip1->TabIndex = 2;
			this->statusStrip1->Text = L"statusStrip1";
			// 
			// toolStripStatusLabel1
			// 
			this->toolStripStatusLabel1->Name = L"toolStripStatusLabel1";
			this->toolStripStatusLabel1->Size = System::Drawing::Size(411, 17);
			this->toolStripStatusLabel1->Text = L"Rigth click: stop drawing for complex figures / toggle enclosing in edit mode";
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(748, 541);
			this->Controls->Add(this->statusStrip1);
			this->Controls->Add(this->pictureBox1);
			this->Controls->Add(this->menuStrip1);
			this->ForeColor = System::Drawing::SystemColors::ControlText;
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"MyForm";
			this->Text = L"������ 3. ������ ����� � B-�������. �������� ��������";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			this->statusStrip1->ResumeLayout(false);
			this->statusStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	private:
		// ������������ ������ ���������
		enum class DrawingMode
		{
			Line = 0,
			Circle,
			Ellipse,
			Fill,
			Polygon,
			ClippingArea,
			SimpleBezier,
			ComplexBezier,
			SimpleBSpline,
			ComplexBSpline,
		};

		DrawingMode mode;
		Bitmap^ bitmap;
		Graphics^ g;
		bool editMode;

		DrawingContainer^ drawing;
		
		Figure^ currentFigure; /**< ������, ������� � ������ ������ ������ ������. */

		Random^ random = gcnew Random();

		Void SetMode(DrawingMode newMode) {
			mode = newMode;
			toolStripComboBox1->SelectedIndex = (int)mode;
		}

		// �������������
		Void Start() {
			bitmap = gcnew Bitmap(1920, 1080);
			g = Graphics::FromImage(bitmap);
			pictureBox1->Image = bitmap;
			SetMode(DrawingMode::Line);

			drawing = gcnew DrawingContainer(g, pictureBox1->BackColor);
			drawing->drawingContainerUpdateHandler += gcnew DrawingContainer::DrawingContainerUpdateHandler(this, &MyForm::OndrawingContainerUpdateHandler);

			Drawing::Font^ font = gcnew Drawing::Font("Segoe UI", 30);
			g->DrawString("Click and hold to draw!", font, Brushes::Gray, 100, 100);
		}

		void OndrawingContainerUpdateHandler()
		{
			pictureBox1->Refresh();
		}

		Figure^ CreateFigure(Point^ p1, Point^ p2)
		{
			Figure^ f = CreateFigure(p1);
			f->point2 = p2;

			return f;
		}

		// ������� ������ � ������ ������
		Figure^ CreateFigure(Point^ point1) {
			Pen^ p = gcnew Pen(colorToolStripMenuItem->BackColor);

			Figure^ f;
			switch (mode)
			{
			case DrawingMode::Line:
				f = gcnew Line();
				break;
			case DrawingMode::Circle:
				f = gcnew Circle();
				break;
			case DrawingMode::Ellipse:
				f = gcnew Ellipse();
				break;
			case DrawingMode::Fill:
				f = gcnew Filling(bitmap);
				break;
			case DrawingMode::Polygon:
				f = gcnew Polygon(point1);
				break;
			case DrawingMode::ClippingArea:
				f = gcnew LineClipper();
				break;
			case DrawingMode::SimpleBezier:
				f = gcnew Bezier(point1);
				break;
			case DrawingMode::ComplexBezier:
				f = gcnew BezierComplex(point1);
				break;
			case DrawingMode::SimpleBSpline:
				f = gcnew BSpline(point1);
				break;
			/*case DrawingMode::ComplexBSpline:
				f = gcnew BezierComplex(point1);
				break;*/
			}
			f->point1 = point1;
			f->point2 = point1;
			f->g = g;
			f->p = p;
			return f;
		}

		bool BitmapContainsPoint(int x, int y)
		{
			return x >= 0 && x < bitmap->Size.Width
				&& y >= 0 && y < bitmap->Size.Height;
		}

		// �������� � ����������� ����� ������ �� �������� �����
		Void pictureBox1_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			if (editMode)
			{
				drawing->MouseDown(e->Location);
				return;
			}

			if (currentFigure)
				return;

			currentFigure = CreateFigure(e->Location);
			if (mode != DrawingMode::ClippingArea)
				drawing->Add(currentFigure);
			else
				drawing->clipper = (LineClipper ^)currentFigure;
		}

		Void pictureBox1_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			if (editMode)
			{
				drawing->MouseUp(e->Location, e->Button == System::Windows::Forms::MouseButtons::Right);
				return;
			}

			if (currentFigure && Polygon::typeid->IsAssignableFrom(currentFigure->GetType()))
			{
				Polygon^ pol = (Polygon ^)currentFigure;
				if (e->Button == System::Windows::Forms::MouseButtons::Left)
				{
					pol->AddVertex(pol->point2);
				}
				else if (e->Button == System::Windows::Forms::MouseButtons::Right)
				{
					pol->EndDrawing();
					currentFigure = nullptr;
					drawing->DrawFigures();
				}
			}
			else
				currentFigure = nullptr;
		}

		Void pictureBox1_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			if (editMode)
			{
				drawing->MouseMove(e->Location);
				return;
			}

			if (currentFigure && BitmapContainsPoint(e->Location.X, e->Location.Y)) {
				currentFigure->point2 = e->Location;

				drawing->DrawFigures();
			}
		}

		// ����������� ������� �������
		Void toolStripComboBox1_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {
			mode = (DrawingMode)toolStripComboBox1->SelectedIndex;
		}

		Void undoToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			if (currentFigure && Polygon::typeid->IsAssignableFrom(currentFigure->GetType()))
			{
				Polygon^ pol = (Polygon ^)currentFigure;
				pol->Undo();
				drawing->DrawFigures();
			}
			else
				drawing->DeleteLastFigure();
		}

		Void clearToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			currentFigure = nullptr;
			drawing->Clear();
			drawing->DrawFigures();
		}

		Void aboutToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			MessageBox::Show("\
��������: �������� ��������, ���-153\n\
\n\
����� ����������: Visual Studio 2017\n\
�������: Windows 10\n\
��������� ���������� :\n\
  CPU: Intel Core i5 7600K 4.8GHz\n\
  GPU: Asus GeForce 1060 6GB\n\
_____________________________________________________________\n\n\
������ 1. ��������� ����������\n\
\n����: 18.09.2018\n\
\n\
�����������:\n\
  - ����������� git �����������\n\
  - ����������� ��������� � ��������� � ������� ��������� ��� �������� ���������\n\
  - ���������� ���� �������� � ������� ������� � ����������� �����\n\
  - ��������� ������� ��� ����������� ������� ������ ����\n\
  - ���������� �������� ����������\n\
  - ���������� ������ / ���������� / �������\n\
  - ����������� ���������� ����� ������, ������������ ���������� ������� Graphics\n\
  - ����������� ������� ��������� ������������ ������\n\
_____________________________________________________________\n\n\
������ 2. ������� � ��������� ���������\n\n\
�������:\n\
  - ���������� ������� ���������\n\
  - ������� � ����������� ������\n\
  - ����� ����� ������� � �����\n\
\n\
��������� ��������:\n\
  - ���./����. ����� ��������� ����� ���� Settings\n\
  - ������� ������� ��������� ������������ ���� (Figure: Clipping Area)\n\
  - ��������� �������� ������ ������ � ����������� �� �� �����\n\
_____________________________________________________________\n\n\
������ 3. ������� � ��������� ���������\n\n\
������ ����� (��� ���������, ���������, �������������� ���������)\n\
B-������� (���� ��������, ���������, ��������������)\n\
");
		}

		Void drawSecondLineWithGraphicsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

		}
		Void drawFiguresWithBuiltinGraphicsFunctionsToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			drawBuiltinMenuItem->Checked = !drawBuiltinMenuItem->Checked;
			Figure::builtinShow = drawBuiltinMenuItem->Checked;
			drawing->DrawFigures();
		}
		Void drawRectangleAroundEllipseToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			drawRectangleMenuItem->Checked = !drawRectangleMenuItem->Checked;
			Ellipse::drawRectangle = drawRectangleMenuItem->Checked;
			drawing->DrawFigures();
		}
		Void generateRandomFiguresToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			for (int i = 0; i < 30; i++)
			{
				int minX = 10,
					maxX = pictureBox1->Size.Width - 10,
					minY = 10,
					maxY = pictureBox1->Size.Height - 10;
				Point^ p1 = gcnew Point(random->Next(minX, maxX), random->Next(minY, maxY));
				Point^ p2 = gcnew Point(random->Next(minX, maxX), random->Next(minY, maxY));

				int type = random->Next(0, 3);

				Figure^ f;
				if (type == 0)
					f = gcnew Line();
				else if (type == 1)
					f = gcnew Circle();
				else
					f = gcnew Ellipse();

				f->point1 = p1;
				f->point2 = p2;
				f->g = g;
				f->p = Pens::Coral;

				drawing->Add(f);
			}
		}
		Void verboseToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			verboseToolStripMenuItem->Checked = !verboseToolStripMenuItem->Checked;
			drawing->verboseDrawing = verboseToolStripMenuItem->Checked;
			drawing->DrawFigures();
		}
		Void drawPixelsForFillingBetweenToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			drawPixelsForFillingBetweenToolStripMenuItem->Checked = !drawPixelsForFillingBetweenToolStripMenuItem->Checked;
			drawing->verboseFillingPoints = drawPixelsForFillingBetweenToolStripMenuItem->Checked;
			drawing->DrawFigures();
		}
		Void fillFiguresToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			fillFiguresToolStripMenuItem->Checked = !fillFiguresToolStripMenuItem->Checked;
			drawing->fillFigures = fillFiguresToolStripMenuItem->Checked;
			drawing->DrawFigures();
		}
		Void clippingModeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			clippingModeToolStripMenuItem->Checked = !clippingModeToolStripMenuItem->Checked;
			drawing->clippingMode = clippingModeToolStripMenuItem->Checked;
			drawing->DrawFigures();
		}
		Void colorToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			colorDialog1->Color = colorToolStripMenuItem->BackColor;
			System::Windows::Forms::DialogResult result = colorDialog1->ShowDialog();
			if (result == System::Windows::Forms::DialogResult::OK) {
				colorToolStripMenuItem->BackColor = colorDialog1->Color;
			}
		}
		Void editModeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			editMode = editModeToolStripMenuItem->Checked;
			editModeToolStripMenuItem->BackColor = editMode ? Color::FromKnownColor(KnownColor::ActiveCaption) : Color::FromKnownColor(KnownColor::Control);
		}
};
}


