#pragma once

#include "Figure.h"

namespace hse_graphics_1_brez
{
	using namespace System::Drawing;

	double PLen(Point^ a)
	{
		return Figure::Distance(gcnew Point(0, 0), a);
	}

	Point^ PSum(Point^ a, double mult, Point^ b)
	{
		return gcnew Point(
			Math::Round(a->X + b->X * mult),
			Math::Round(a->Y + b->Y * mult)
		);
	}

	Point^ PLerp(Point^ a, Point^ b, double c)
	{
		Point^ prevM2ToC = PSum(b, -1, a);
		return PSum(a, c, prevM2ToC);
	}

	Point^ PWithLen(Point ^a, double l)
	{
		return PSum(gcnew Point(0, 0), l / PLen(a), a);
	}
}