#pragma once

#include "Figure.h"

namespace hse_graphics_1_brez
{
	using namespace System;
	using namespace System::Drawing;

	// ����� ��� ��������� ��������������
	ref class Rect : Figure
	{

	public:

		Rect()
		{
			this->canBeFilled = false;
		}

		// �������������� ����� ���������
		Void DrawFigure() override
		{
			DrawRectangle(p, point1, point2);
		}

		Rectangle^ GetRectangle()
		{
			return RectForPoints(point1, point2);
		}
	};
}

