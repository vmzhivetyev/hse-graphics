#pragma once

#include "Polygon.h"

namespace hse_graphics_1_brez
{
	ref class BSpline : Polygon
	{

	public:

		BSpline(Point^ p) : Polygon(p) 
		{
			this->closed = false;
			this->canBeFilled = false;
		}

	protected:

		int pointRadius = 6;
		int editing = -1;

		// �������������� ����� ���������
		Void DrawFigure() override
		{
			Pen^ tempPen = p;
			p = Pens::LightGray;
			Polygon::DrawFigure();
			p = tempPen;

			if (vertex->Count > 0 && point2)
			{
				vertex[vertex->Count - 1] = point2;
			}

			for each (Point^ p in vertex)
			{
				DrawEllipse(p->X, p->Y, 3, 3);
			}

			drawnPoints->Clear();
			DrawBSpline(vertex);
		}

		Void DrawBSpline(ArrayList^ arr)
		{
			Point^ prev;

			for (int i = 0; i < arr->Count - (closed ? 0 : 3); i++)
			{
				for (double t = 0; t <= 1; t += 0.005)
				{
					Point^ p = P(t, arr, i);
					if (prev)
						DrawLine(prev, p);
					prev = p;
				}
			}
			if (!closed && arr->Count >= 4)
			{
				Point^ p = P(1, arr, arr->Count - 4);
				DrawLine(prev, p);
			}
		}

		Point^ P(double t, ArrayList^ arr, int start)
		{
			int x = B(t, arr, true, start);
			int y = B(t, arr, false, start);

			return gcnew Point(x, y);
		}

		int B(double t, ArrayList^ arr, bool isX, int start)
		{
			Point^ p0 = ((Point^)arr[start + 0]);
			Point^ p1 = ((Point^)arr[(start + 1) % arr->Count]);
			Point^ p2 = ((Point^)arr[(start + 2) % arr->Count]);
			Point^ p3 = ((Point^)arr[(start + 3) % arr->Count]);
			int c0 = isX ? p0->X : p0->Y;
			int c1 = isX ? p1->X : p1->Y;
			int c2 = isX ? p2->X : p2->Y;
			int c3 = isX ? p3->X : p3->Y;

			double result = 1.0 / 6 * (Math::Pow(1 - t, 3) * c0 + (3 * t*t*t - 6 * t*t + 4) * c1 + (-3 * t*t*t + 3 * t*t + 3 * t + 1) * c2 + t*t*t*c3);
			return Math::Round(result);
		}


	public:

		bool MouseDown(Point^ loc) override
		{
			if (editing < 0)
			{
				for each (Point^ p in vertex)
				{
					double d = Figure::Distance(loc, p);
					if (d <= pointRadius)
					{
						editing = vertex->IndexOf(p);
					}
				}
			}

			if (editing >= 0)
			{
				if (Figure::Distance(loc, (Point^)vertex[editing]) > pointRadius)
				{
					editing = -1;
					return true;
				}
			}

			return false;
		}

		bool MouseUp(Point^ loc, bool rClick) override
		{
			if (editing >= 0)
			{
				editing = -1;

				if (rClick)
					closed = !closed;

				return true;
			}
			else
			{
				return false;
			}
		}

		bool MouseMove(Point^ loc) override
		{
			if (editing >= 0) {
				vertex[editing] = loc;
				return true;
			}
			return false;
		}
	};
}

