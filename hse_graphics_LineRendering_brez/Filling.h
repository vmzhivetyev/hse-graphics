#pragma once

#include "Figure.h"

namespace hse_graphics_1_brez
{
	using namespace System::Collections;

	// ����� ��� ��������� �����
	ref class Filling : Figure
	{
	public:
		Bitmap^ bitmap;
		Stack^ stack = gcnew Stack;

		Filling(Bitmap^ bitmap)
		{
			this->bitmap = bitmap;
			canBeFilled = false;
		}

		// �������������� ����� ���������
		Void DrawFigure() override
		{
			isDrawing = false;

			stack->Clear();
			Point^ startPoint = point2;

			stack->Push(startPoint);
			int startColor = GetColor(startPoint->X, startPoint->Y);

			if (startColor == p->Color.ToArgb())
			{
				return;
			}

			while (stack->Count > 0)
			{
				Point^ pixel = (Point^)stack->Pop();

				DrawPoint(pixel->X, pixel->Y);

				// ������ �����
				int minX = FillLine(startColor, pixel->X, pixel->Y, stack, -1);
				int maxX = FillLine(startColor, pixel->X, pixel->Y, stack, 1);

				dc->RefreshImage();

				// ������ ������� ����� � ������ � ������� ����� ����������� �����
				ScanLine(stack, startColor, minX, maxX, pixel->Y + 1);
				ScanLine(stack, startColor, minX, maxX, pixel->Y - 1);
			}

			print("Filling from start point DONE.");
		}

	private:
		bool BitmapContainsPoint(int x, int y)
		{
			return x >= 0 && x < bitmap->Size.Width
				&& y >= 0 && y < bitmap->Size.Height;
		}

		int GetColor(int x, int y)
		{
			return bitmap->GetPixel(x, y).ToArgb();
		}

		int FillLine(int startColor, int x, int y, Stack^ stack, int step)
		{
			while (true)
			{
				x += step;
				if (BitmapContainsPoint(x, y))
				{
					int col = GetColor(x, y);
					if (col == startColor) {
						DrawPoint(x, y);
						continue;
					}
				}
				return x - step;
			}
		}

		void print(String^ s)
		{
			System::Diagnostics::Debug::WriteLine(s);
		}

		void print(String^ s, Stack^ st)
		{
			//System::Diagnostics::Debug::Write(s + " ");
			//System::Diagnostics::Debug::WriteLine(st->Peek());
		}

		void ScanLine(Stack^ stack, int startColor, int minX, int maxX, int y)
		{
			if (!BitmapContainsPoint(minX, y))
			{
				return;
			}

			int prevColor = GetColor(minX, y);
			for (int x = minX + 1; x <= maxX; ++x)
			{
				if (!BitmapContainsPoint(x, y) && prevColor == startColor)
				{
					stack->Push(gcnew Point(x - 1, y));
					print("end of screen", stack);
					return;
				}

				int col = GetColor(x, y);
				// ������ � ���� ��� ���������� �����
				if (prevColor == startColor && col != prevColor)
				{
					stack->Push(gcnew Point(x - 1, y));
					print("different color", stack);
				}

				prevColor = col;
			}

			if (prevColor == startColor)
			{
				stack->Push(gcnew Point(maxX, y));
				print("end of line", stack);
			}
		}
	};
}



