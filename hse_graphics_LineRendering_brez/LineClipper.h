#pragma once

#include "Rect.h"

namespace hse_graphics_1_brez
{
	using namespace System;
	using namespace System::Collections;
	using namespace System::Drawing;

	// ����� ��� ��������� ����������
	ref class LineClipper : Rect
	{

	public:
		enum class ClipResult
		{
			BadArguments,
			Invisible,
			Visible,
			Clipped
		};

		Rectangle^ rect;
		Point^ p1;
		Point^ p2;

		ClipResult Clip()
		{
			if (!rect || !p1 || !p2)
				return ClipResult::BadArguments;

			rect = GetRectangle();

			X1 = rect->X;
			X2 = X1 + rect->Width - 1;
			Y1 = rect->Y;
			Y2 = Y1 + rect->Height - 1;

			int minX = Math::Min(p1->X, p2->X);
			int maxX = Math::Max(p1->X, p2->X);
			int minY = Math::Min(p1->Y, p2->Y);
			int maxY = Math::Max(p1->Y, p2->Y);
			lineRect = gcnew Rectangle(
				minX, 
				minY, 
				maxX - minX + 1, 
				maxY - minY + 1);

			return InternalClip();
		}
		

	private:

		int X1, X2, Y1, Y2;
		Rectangle^ lineRect;

		int outCode(Point^ p)
		{
			int code = 0;
			if (p->X < X1) code |= 0x01;
			if (p->X > X2) code |= 0x02;
			if (p->Y < Y1) code |= 0x04;
			if (p->Y > Y2) code |= 0x08;
			return code;
		}

		Void AddUniquePoint(double x, double y, ArrayList^ list)
		{
			Point^ p = gcnew Point(Math::Round(x), Math::Round(y));
			if (!list->Contains(p) && 
				rect->Contains(*p) && 
				lineRect->Contains(*p))
				list->Add(p);
		}

		bool RemoveExcessive(ArrayList^ intersects)
		{
			for each (Point^ ip in intersects)
			{
				for each (Point^ jp in intersects)
				{
					if (ip == jp) continue;

					double dist = Figure::Distance(ip, jp);
					if (dist <= 1)
					{
						intersects->Remove(ip);
						return true;
					}
				}
			}
			return false;
		}

		ClipResult InternalClip()
		{
			int code1 = outCode(p1);
			int code2 = outCode(p2);

			if (code1 == 0x0 && code2 == 0x0)
			{
				return ClipResult::Visible;
			}
			else if ((code1 & code2) != 0x0)
			{
				return ClipResult::Invisible;
			}
			else
			{
				// partly visible

				// K of our line
				double m = (double)(p2->Y - p1->Y) / (p2->X - p1->X);

				// calc coordinates of intersections with each rect edge line 
				// (not constrained with rect edge size)
				double yl = (m * (X1 - p1->X) + p1->Y);
				double yr = (m * (X2 - p1->X) + p1->Y);
				double xt = (p1->X + 1 / m * (Y1 - p1->Y));
				double xb = (p1->X + 1 / m * (Y2 - p1->Y));


				ArrayList^ intersects = gcnew ArrayList;

				if (xt >= 0) AddUniquePoint(xt, Y1, intersects);
				if (xb >= 0) AddUniquePoint(xb, Y2, intersects);
				if (yl >= 0) AddUniquePoint(X1, yl, intersects);
				if (yr >= 0) AddUniquePoint(X2, yr, intersects);

				while (RemoveExcessive(intersects)) { }

				switch (intersects->Count)
				{
				case 1:
					if (code1 != 0) p1 = (Point^)intersects[0];
					if (code2 != 0) p2 = (Point^)intersects[0];
					break;
				case 2:
					p1 = (Point^)intersects[0];
					p2 = (Point^)intersects[1];
					break;
				}

				return intersects->Count > 0 ? ClipResult::Clipped : ClipResult::Invisible;
			}
		}

	};
}

