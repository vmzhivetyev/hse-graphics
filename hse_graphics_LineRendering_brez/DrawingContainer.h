#pragma once

namespace hse_graphics_1_brez
{
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	ref class DrawingContainer;
	ref class Figure;
	ref class Line;
	ref class LineClipper;
	
	ref class DrawingContainer
	{
	public:
		delegate void DrawingContainerUpdateHandler();
		event DrawingContainerUpdateHandler ^ drawingContainerUpdateHandler;
		Void RefreshImage() {
			if (!verboseDrawing) return;
			drawingContainerUpdateHandler();
		}

		bool verboseDrawing = false;
		bool verboseFillingPoints = false;
		bool fillFigures = false;
		bool clippingMode = false;

		Graphics^ g;
		Color^ clearColor = Color::White; /**< ���� ����. */
		Color^ fillColor = Color::Yellow;

		LineClipper^ clipper;

	private:
		ArrayList^ figures = gcnew ArrayList(); /**< ��� ������������ ������. */

		Void DrawLineInClippingMode(Line^ line);

	public:
		DrawingContainer(Graphics^ g, Color^ clearColor);

		Void DrawFigures();
		Void Add(Figure ^figure);
		Void Clear();
		Void DeleteLastFigure();

		Void MouseDown(Point^ loc);
		Void MouseUp(Point^ loc, bool rClick);
		Void MouseMove(Point^ loc);
	};
}