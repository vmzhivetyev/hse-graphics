#pragma once

#include "Figure.h"
#include "SteppableInt.h"
#include "DrawingContainer.h"

namespace hse_graphics_1_brez
{
	using namespace System::Threading;

	ref class Comparer : IComparer
	{
	public:
		virtual int Comparer::Compare(System::Object ^x, System::Object ^y)
		{
			Point^ a = (Point^)x;
			Point^ b = (Point^)y;
			
			if (a->Y > b->Y)
				return 1;
			if (a->Y < b->Y)
				return -1;

			if (a->X > b->X)
				return 1;
			if (a->X < b->X)
				return -1;

			return 0;
		}
	};

	Void Figure::Draw()
	{
		//System::Diagnostics::Debug::WriteLine("Drawing figure" + this);

		uniqueYPoints->Clear();
		drawnPoints->Clear();
		lastDrawnPointY = -1;

		isDrawing = true;
		DrawFigure();
		isDrawing = false;

		if (dc->verboseFillingPoints)
		{
			Brush^ b = gcnew SolidBrush(Color::Black);
			for (int i = 0; i < uniqueYPoints->Count; i++)
			{
				Point^ vp = (Point^)uniqueYPoints[i];
				g->FillRectangle(b, vp->X, vp->Y, 1, 1);
			}
		}
	}

	Void Figure::DrawPoint(int x, int y)
	{
		Brush^ b = gcnew SolidBrush(p->Color);
		g->FillRectangle(b, x, y, 1, 1);

		if (isDrawing)
		{
			Point^ p = gcnew Point(x, y);
			drawnPoints->Add(p);
			if (y != lastDrawnPointY)
			{
				//if (!drawnPoints->Contains(gcnew Point(x, y)))
				{
					uniqueYPoints->Add(p);
				}
			}
			lastDrawnPointY = y;
			dc->RefreshImage();
		}
	}

	Rectangle^ Figure::RectForPoints(Point^ p1, Point^ p2)
	{
		int dx = Math::Abs((p1->X - p2->X));
		int dy = Math::Abs((p1->Y - p2->Y));

		Rectangle^ rect = gcnew Rectangle();
		rect->Location = *(gcnew Point(Math::Min(p1->X, p2->X), Math::Min(p1->Y, p2->Y)));
		rect->Size = *(gcnew Drawing::Size(dx, dy));
		return rect;
	}
	
	Void Figure::DrawRectangle(Pen ^ pen, Point ^ p1, Point ^ p2)
	{
		Rectangle^ rect = RectForPoints(p1, p2);
		g->DrawRectangle(pen, *rect);
	}

	Void Figure::DrawLine(Point ^ from, Point ^ to)
	{
		// ������������� ������ ��������� ���������������� �� ������ ���������� ����� � 
		SteppableInt^ X = gcnew SteppableInt(from->X, to->X);
		SteppableInt^ Y = gcnew SteppableInt(from->Y, to->Y);

		DrawPoint(X->value, Y->value);

		int dx = X->GetDelta();
		int dy = Y->GetDelta();
		int maxDelta = Math::Max(dx, dy);
		int minDelta = Math::Min(dx, dy);
		bool dx_is_max = dx == maxDelta;

		// �� ����� ��� ������ ������ - ��� max, �������������� �� � � �
		SteppableInt^ min = dx_is_max ? Y : X;
		SteppableInt^ max = dx_is_max ? X : Y;

		int error = 0; // ����� ���������� ���������� �� ��� � ����������� �������
		int errorEqualToHalfOfCoordinate = maxDelta; // �������� ������, ������� ������������ ���������� ���������� �� 0.5
		int errorStep = minDelta * 2; // ��������, �� ������� ����� ������������� ����� �� ������ ����

		for (int i = 0; i < maxDelta; i++)
		{
			max->DoStep(); // ������ �������� ����������� ���������� � ������������ ������� �� 1

			error += errorStep;
			if (error > errorEqualToHalfOfCoordinate) // �������� ������ �� ����� ���������� ��� 0.5
			{
				error -= errorEqualToHalfOfCoordinate * 2; // ����� �������� �� ������ ���������� ���������� 1
				min->DoStep(); // � ������� ����������� ���������� �� 1
			}

			DrawPoint(X->value, Y->value);
		}
	}

	Void Figure::DrawEllipse(int cx, int cy, int a, int b)
	{
		int x = 0;
		int y = b;
		int A = a*a;
		int B = b*b;

		int d = A + B - 2 * A * b;

		ArrayList^ ellipsePoints = gcnew ArrayList;

		while (true)
		{
			DrawPoint(cx + x, cy + y);
			ellipsePoints->Add(gcnew Point(x, y));

			if (y <= 0)
			{
				break;
			}

			if (d < 0)
			{
				int hMinusD = 2 * d + 2 * y * A - A;
				if (hMinusD <= 0)
				{
					++x;
					d += 2 * x * B + B;
					continue;
				}
			}
			else if (d > 0)
			{
				int dMinusV = 2 * d - 2 * x * B - B;
				if (dMinusV > 0)
				{
					--y;
					d += A - 2 * y * A;
					continue;
				}
			}

			++x;
			--y;
			d += (2 * x * B + B) + (-2 * y * A + A);
		}

		int pointCount = ellipsePoints->Count;
		for (int i = 0; i < pointCount; i++)
		{
			Point^ dp = (Point ^)ellipsePoints[i];
			DrawPoint(cx - dp->X, cy + dp->Y);
		}
		pointCount -= 1;
		for (int i = 0; i < pointCount; i++)
		{
			Point^ dp = (Point ^)ellipsePoints[i];
			DrawPoint(cx + dp->X, cy - dp->Y);
		}
		for (int i = 0; i < pointCount; i++)
		{
			Point^ dp = (Point ^)ellipsePoints[i];
			DrawPoint(cx - dp->X, cy - dp->Y);
		}
	}

	Void Figure::Draw(Color^ fillColor)
	{
		Draw();
		if (canBeFilled && fillColor)
		{
			Fill(fillColor);
			Draw();
		}
	}

	Void Figure::Fill(Color^ fillColor)
	{
		if (!canBeFilled)
		{
			return;
		}

		uniqueYPoints->Sort(gcnew Comparer);

		Pen^ realPen = p;
		p = gcnew Pen(*fillColor);

		for (int i = 0; i < uniqueYPoints->Count - 1; i+=2)
		{
			Point^ a = (Point ^)uniqueYPoints[i];
			Point^ b = (Point ^)uniqueYPoints[i+1];
			if (a->Y != b->Y)
			{
				p = gcnew Pen(Color::Beige);
				--i;
				continue;
			}
			DrawLine(a, b);
			dc->RefreshImage();
			if (dc->verboseDrawing)
			{
				Thread::Sleep(100);
			}
		}

		p = realPen;
	}
}