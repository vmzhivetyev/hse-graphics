#pragma once

#include "Polygon.h"

namespace hse_graphics_1_brez
{
	ref class Bezier : Polygon
	{

	public:
		
		Bezier(Point^ p) : Polygon(p)
		{
			this->closed = false;
			this->canBeFilled = false;
		}

	protected:

		bool useCastelgio = false;

		// �������������� ����� ���������
		Void DrawFigure() override
		{
			Pen^ tempPen = p;
			p = Pens::LightGray;
			Polygon::DrawFigure();
			p = tempPen;

			if (vertex->Count > 0 && point2)
			{
				vertex[vertex->Count - 1] = point2;
			}

			for each (Point^ p in vertex)
			{
				DrawEllipse(p->X, p->Y, 3, 3);
			}

			drawnPoints->Clear();

			DrawBezier(vertex);

			if (builtinShow)
			{
				p = Pens::Red;
				useCastelgio = true;
				DrawBezier(vertex);
				useCastelgio = false;
				p = tempPen;
			}
		}

		Void DrawBezier(ArrayList^ arr)
		{
			Point^ prev;

			for (double t = 0; t <= 1; t += 0.005)
			{
				Point^ p = P(t, arr);

				if (prev)
					DrawLine(prev, p);
				prev = p;
			}
			Point^ p = P(1, arr);
			DrawLine(prev, p);
		}

		Void DrawBezierBinary(double a, double b)
		{
			if (a >= b)
				return;

			double t = (a + b) / 2;
			Point^ p = P(t, vertex);
			if (drawnPoints->Contains(p))
			{
				return;
			}

			DrawPoint(p->X, p->Y);

			//dc->RefreshImage();

			DrawBezierBinary(a, t);
			DrawBezierBinary(t, b);
		}

		Point^ P(double t, ArrayList^ arr)
		{
			int x = B(t, arr, true);
			int y = B(t, arr, false);

			if (useCastelgio)
			{
				x = BC(t, arr, true) + 5;
				y = BC(t, arr, false) + 5;
			}

			return gcnew Point(x, y);
		}

		int BC(double t, ArrayList^ arr, bool isX)
		{
			int n = arr->Count;
			ArrayList^ a = gcnew ArrayList(arr->Count);
			for (int i = 0; i < n; i++)
			{
				a->Add((double)(isX ? ((Point^)arr[i])->X : ((Point^)arr[i])->Y));
			}

			while (n > 1)
			{
				n--;
				for (int i = 0; i < n; i++)
				{
					a[i] = (double)a[i] + ((double)a[i + 1] - (double)a[i]) * t;
				}
			}
			return Math::Round((double)a[0]);
		}

		int B(double t, ArrayList^ arr, bool isX)
		{
			double result = 0;
			int n = arr->Count;
			for (int i = 0; i < n; i++)
			{
				int xy = isX ? ((Point^)arr[i])->X : ((Point^)arr[i])->Y;
				double c = C(n - 1, i);
				double p1 = Math::Pow(t, i);
				double p2 = Math::Pow(1 - t, n - i - 1);
				result += xy * c * p1 * p2;
			}
			return Math::Round(result);
		}

		double C(double n, double k) {
			if (k == 0)
				return 1;
			return C(n - 1, k - 1) * n / k;
		}
	};
}

