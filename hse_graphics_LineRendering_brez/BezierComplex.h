#pragma once

#include "Bezier.h"
#include "PointMath.h"

namespace hse_graphics_1_brez
{
	ref class Connection
	{
	public:
		Point^ p;

		Point^ m1;
		Point^ m2;

		Void SetP(Point^ _p)
		{
			Point^ offset1 = PSum(m1, -1, p);
			Point^ offset2 = PSum(m2, -1, p);

			p = _p;

			m1 = PSum(p, 1, offset1);
			m2 = PSum(p, 1, offset2);
		}

		Void SetM1(Point^ _m1)
		{
			if (p->Equals(_m1))
				return;

			Point^ offset2 = PSum(m2, -1, p);
			double l2 = PLen(offset2);

			m1 = _m1;

			Point^ offset1 = PSum(m1, -1, p);
			m2 = PSum(p, -1, PWithLen(offset1, l2));
		}

		Void SetM2(Point^ _m2)
		{
			if (p->Equals(_m2))
				return;

			Point^ offset1 = PSum(m1, -1, p);
			double l1 = PLen(offset1);

			m2 = _m2;

			Point^ offset2 = PSum(m2, -1, p);
			m1 = PSum(p, -1, PWithLen(offset2, l1));
		}
	};

	// ����� ��� ��������� ����������
	ref class BezierComplex : Bezier
	{

	public:
		ArrayList^ connections = gcnew ArrayList;
		Connection^ editingConnection;
		bool edP, ed1, ed2;
		int pointRadius = 5;

		BezierComplex(Point^ p) : Bezier(p) { }

		void AddVertex(Point^ p) override
		{
			if (vertex->Count > 0)
			{
				Connection^ c = gcnew Connection;
				c->p = (Point^)vertex[vertex->Count - 1];
				c->m1 = c->p;
				c->m2 = c->p;

				if (connections->Count > 0)
				{
					Connection^ prev = (Connection^)connections[connections->Count - 1];
					if (connections->Count == 1)
					{
						prev->m2 = PLerp(prev->p, c->p, 0.4);
						prev->m1 = PLerp(prev->p, c->p, -0.4);
					}
					c->m1 = PLerp(prev->m2, c->p, 0.6);
					c->m2 = PLerp(prev->m2, c->p, 1.4);
				}

				connections->Add(c);
			}

			Bezier::AddVertex(p);
		}

		Void Undo() override
		{
			Bezier::Undo();

			if (connections->Count > 0)
				connections->RemoveAt(connections->Count - 1);
		}

		Void EndDrawing() override
		{
			point2 = nullptr;
			Bezier::Undo();
		}

		Void FillCircle(Brush^ b, Point^ center, int r)
		{
			int x = center->X - r;
			int y = center->Y - r;
			g->FillEllipse(b, x, y, r*2, r*2);
		}

		// �������������� ����� ���������
		Void DrawFigure() override
		{
			for (int i = 0; i < connections->Count; i++)
			{
				Connection^ c = (Connection^)connections[i];
				vertex[i] = c->p;
			}

			Pen^ tempPen = p;
			p = Pens::LightGray;
			Polygon::DrawFigure();
			p = tempPen;

			if (vertex->Count > 0 && point2)
			{
				vertex[vertex->Count - 1] = point2;
			}

			for each (Connection^ c in connections)
			{
				DrawEllipse(c->p->X, c->p->Y, 3, 3);
			}

			/*for each (Connection^ c in connections)
			{
				p = Pens::Blue;
				DrawEllipse(c->m1->X, c->m1->Y, 3, 3);
				p = Pens::Orange;
				DrawEllipse(c->m2->X, c->m2->Y, 3, 3);
			}*/
			if (editingConnection)
			{
				p = Pens::Orange;
				FillCircle(Brushes::Orange, editingConnection->m1, 6);
				FillCircle(Brushes::Orange, editingConnection->m2, 6);
				//DrawEllipse(editingConnection->m1->X, editingConnection->m1->Y, pointRadius, pointRadius);
				//DrawEllipse(editingConnection->m2->X, editingConnection->m2->Y, pointRadius, pointRadius);
				DrawLine(editingConnection->m1, editingConnection->m2);
				p = tempPen;
			}

			drawnPoints->Clear();

			ArrayList^ pointsForPart = gcnew ArrayList(4);
			for (int i = 0; i < connections->Count - 1; i++)
			{
				Connection^ c1 = (Connection^)connections[i];
				Connection^ c2 = (Connection^)connections[i + 1];

				pointsForPart->Clear();

				pointsForPart->Add(c1->p);
				pointsForPart->Add(c1->m2);
				pointsForPart->Add(c2->m1);
				pointsForPart->Add(c2->p);

				DrawBezier(pointsForPart);
			}

			if (closed)
			{
				Connection^ c1 = (Connection^)connections[connections->Count - 1];
				Connection^ c2 = (Connection^)connections[0];

				pointsForPart->Clear();

				pointsForPart->Add(c1->p);
				pointsForPart->Add(c1->m2);
				pointsForPart->Add(c2->m1);
				pointsForPart->Add(c2->p);

				DrawBezier(pointsForPart);

			}
		}

		bool MouseDown(Point^ loc) override
		{
			if (!editingConnection)
			{
				for each (Connection ^ c in connections)
				{
					double d = Figure::Distance(loc, c->p);
					if (d <= pointRadius)
					{
						editingConnection = c;
					}
				}
			}

			if (editingConnection)
			{
				edP = Figure::Distance(loc, editingConnection->p) <= pointRadius;
				if (edP) return true;
				ed1 = Figure::Distance(loc, editingConnection->m1) <= pointRadius;
				if (ed1) return true;
				ed2 = Figure::Distance(loc, editingConnection->m2) <= pointRadius;
				if (ed2) return true;

				editingConnection = nullptr;
				return true;
			}

			return false;
		}

		bool MouseUp(Point^ loc, bool rClick) override
		{
			if (editingConnection)
			{
				ed1 = false;
				ed2 = false;
				edP = false;

				if (rClick)
					closed = !closed;

				return true;
			}
			else
			{
				return false;
			}
		}

		bool MouseMove(Point^ loc) override
		{
			if (edP) {
				editingConnection->SetP(loc);
				return true;
			}
			if (ed1) {
				editingConnection->SetM1(loc);
				return true;
			}
			if (ed2) {
				editingConnection->SetM2(loc);
				return true;
			}
			return false;
		}
	};
}

