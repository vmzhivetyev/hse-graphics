#pragma once

#include "Figure.h"

namespace hse_graphics_1_brez
{
	using namespace System::Collections;

	// ����� ��� ��������� ������
	private ref class Polygon : Figure
	{
	protected:
		ArrayList^ vertex = gcnew ArrayList;

	public:
		bool closed = true;

		virtual Void AddVertex(Point^ p) 
		{
			vertex->Add(p);
		}

		virtual Void EndDrawing()
		{
			//Undo();
			point2 = nullptr;
		}

		virtual Void Undo()
		{
			if (vertex->Count > 0)
				vertex->RemoveAt(vertex->Count - 1);
		}

		Polygon(Point^ p)
		{
			AddVertex(p);
			//canBeFilled = false;
		}

		int CycledClamp(int v, int max)
		{
			return v % max;
		}

		int followingPoint(int index)
		{
			int next = (index + 1) % vertex->Count;

			if (canBeFilled)
			{
				// skipping double horizontal vertices
				if (vertex->Count >= 4)
				{
					int i3 = (next + 1) % vertex->Count;

					Point^ p1 = VertexAt(index, 0);
					Point^ p2 = VertexAt(next, 0);
					Point^ p3 = VertexAt(i3, 0);

					if (verticalDirection(p1, p2) == 0 && verticalDirection(p2, p3) == 0)
					{
						next = followingPoint(next);
					}
				}
			}

			return next;
		}

		int verticalDirection(Point^ from, Point^ to)
		{
			if (from->Y > to->Y)
				return -1;
			if (from->Y < to->Y)
				return 1;
			return 0;
		}

		Point^ VertexAt(int i, int minPoints)
		{
			if (vertex->Count < minPoints)
				return nullptr;

			return (Point^)vertex[i];
		}

	protected:

		// �������������� ����� ���������
		Void DrawFigure() override
		{
			if (vertex->Count > 0 && point2)
			{
				vertex[vertex->Count - 1] = point2;
			}

			if (vertex->Count < 2)
				return;

			for (int i = 0; i < vertex->Count - 1 + closed;)
			{
				// drawing an edge

				int i1 = i;
				int i2 = followingPoint(i1);

				Point^ from = VertexAt(i1, 1);
				Point^ to = VertexAt(i2, 1);

				if (!to)
					return;

				DrawLine(from, to);

				if (canBeFilled)
				{
					// checking special cases
					int i3 = followingPoint(i2);
					int i4 = followingPoint(i3);
					Point^ p3 = VertexAt(i3, 2);
					Point^ p4 = VertexAt(i4, 3);
				
					bool toIsVerticalExtrema = p3 && // enough points
						verticalDirection(to, from) == verticalDirection(to, p3);

					bool horizontalEdgeLine = p3 && p4 && // enough points
						verticalDirection(to, p3) == 0 &&
							verticalDirection(to, from) == verticalDirection(p3, p4);

					if (toIsVerticalExtrema || horizontalEdgeLine)
					{
						if (uniqueYPoints->Count > 0)
							uniqueYPoints->RemoveAt(uniqueYPoints->Count - 1);
					}
				}

				if (i2 <= i)
					break;
				i = i2;
			}
			if (uniqueYPoints->Count > 0)
				uniqueYPoints->RemoveAt(0);
		}
	};
}

