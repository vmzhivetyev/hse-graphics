#include "DrawingContainer.h"
#include "Line.h"
#include "LineClipper.h"

namespace hse_graphics_1_brez
{
	DrawingContainer::DrawingContainer(Graphics^ g, Color^ clearColor)
	{
		this->g = g;
		this->clearColor = clearColor;
	}

	// ����� �������������� ��� �������
	Void DrawingContainer::DrawFigures() 
	{
		g->Clear(*clearColor);

		if (clipper)
		{
			clipper->rect = this->clipper->GetRectangle();

			g->DrawRectangle(Pens::LightGray,
				clipper->rect->X,
				clipper->rect->Y,
				clipper->rect->Width - 1,
				clipper->rect->Height - 1);
		}

		for (int i = 0; i < figures->Count; i++) {
			Figure^ f = (Figure^)figures[i];
			f->dc = this;

			if (clippingMode && f->GetType() == Line::typeid)
				DrawLineInClippingMode((Line^)f);
			else
				f->Draw(fillFigures ? fillColor : nullptr);
		}

		drawingContainerUpdateHandler();
	}

	Void DrawingContainer::DrawLineInClippingMode(Line^ line)
	{
		if (!clipper)
		{
			line->Draw();
			return;
		}

		clipper->p1 = line->point1;
		clipper->p2 = line->point2;

		LineClipper::ClipResult result = clipper->Clip();

		Pen^ penTemp = line->p;
		if (result == LineClipper::ClipResult::Invisible) line->p = Pens::Red;
		if (result == LineClipper::ClipResult::Visible) line->p = Pens::Magenta;
		if (result == LineClipper::ClipResult::Clipped) line->p = Pens::Blue;
		line->DrawLine(clipper->p1, clipper->p2);
		line->p = penTemp;
	}

	Void DrawingContainer::Add(Figure ^figure)
	{
		figures->Add(figure);
		DrawFigures();
	}

	Void DrawingContainer::Clear()
	{
		figures->Clear();
		clipper = nullptr;
		DrawFigures();
	}

	Void DrawingContainer::DeleteLastFigure()
	{
		if (figures->Count > 0)
			figures->RemoveAt(figures->Count - 1);
		DrawFigures();
	}

	Void DrawingContainer::MouseDown(Point^ loc)
	{
		for each (Figure^ f in figures)
		{
			if (f->MouseDown(loc))
			{
				DrawFigures();
				return;
			}
		}
	}

	Void DrawingContainer::MouseUp(Point^ loc, bool rClick)
	{
		for each (Figure^ f in figures)
		{
			if (f->MouseUp(loc, rClick))
			{
				DrawFigures();
				return;
			}
		}
	}

	Void DrawingContainer::MouseMove(Point^ loc)
	{
		for each (Figure^ f in figures)
		{
			if (f->MouseMove(loc))
			{
				DrawFigures();
				return;
			}
		}
	}
}