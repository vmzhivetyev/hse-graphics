#pragma once

namespace hse_graphics_1_brez 
{
	using namespace System;

	// ����� ��� ���������� ��������� �������� � ����������� �� ���������� �� ���������
	// (��������� ���������������� �� ����� ������ � ��������� ���������� ������)
	ref class SteppableInt : public Object
	{
	public: int value;
	private: int step;
	private: int delta;

	public: SteppableInt(int from, int to)
	{
		value = from;
		delta = Math::Abs(to - from);
		step = to.CompareTo(from);
	}

	public: int GetDelta()
	{
		return delta;
	}

	public: Void DoStep()
	{
		value += step;
	}

	};
}